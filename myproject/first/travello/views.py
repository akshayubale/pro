from django.shortcuts import render

from .models import Destination



from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Destination 
from .serializers import DestinationSerializer
from rest_framework import status




""""
def index(request):
    dest1=Destination()
    dest1.name="mumbai"
    dest1.img="01-greece.jpg"
    dest1.price=890
    dest1.offer=False

    dest2=Destination()
    dest2.name="hyderabad"
    dest2.img="01-greece.jpg"
    dest2.price=84

    dest3=Destination()
    dest3.name="banglore"
    dest3.img="01-greece.jpg"
    dest3.price=890

    dests={dest1,dest2,dest3}

    return render(request,'index.html',{'dest1':dest1})
"""


def index(request):
    dests=Destination.objects.all()
    return render(request,'index.html',{'dests':dests})


















@api_view(['GET','POST'])
def Destination_list(request):
    if request.method == 'GET':
        obj= Destination.objects.all()
        serializer = DestinationSerializer(obj,many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
         serializer=DestinationSerializer(data=request.data)
         if serializer.is_valid():
             serializer.save()
             return Response(serializer.data,status=status.HTTP_201_CREATED)
         return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET','PUT'])
def Destination_Details(request,pk):
    try:
        obj=Destination.objects.get(id=pk)
    except Destination.DoesNotExist:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    if request.method == 'GET':        
        serializer=DestinationSerializer(obj)
        return Response(serializer.data)
    elif request.method == 'PUT':
        serializer = Destination(obj,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

