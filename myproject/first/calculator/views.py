from django.shortcuts import render
 
from .models import calculator
from .serializer import UserSerializer,CalculatorSerializer

from django.contrib.auth.models import  User

from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token


"""
class UserAuthentication(ObtainAuthToken):   
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,})

"""

from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.conf import settings

from django.dispatch import receiver
from django.contrib.auth import login as django_login,logout as django_logout
from rest_framework.authentication import TokenAuthentication,SessionAuthentication,BasicAuthentication
from rest_framework.permissions import IsAuthenticated,IsAdminUser
from django.contrib.auth import authenticate
from django.contrib.auth.hashers import make_password
import hashlib
"""

for user in User.objects.all():
    Token.objects.get_or_create(user=user)
"""

class register(APIView):
    def post(self,request):    
        try:    
            if(request.data['username'] and request.data['first_name'] and request.data['last_name'] and request.data['email'] and request.data['password']):
                    check_username=User.objects.filter(username=request.data['username'])
                    if(check_username):
                        return Response({'username  is already used'},status=status.HTTP_400_BAD_REQUEST)
                    check_email=User.objects.filter(email=request.data['email'])
                    if(check_email):
                        return Response({'email is already used'},status=status.HTTP_400_BAD_REQUEST)
                    Django_hashed_password=make_password(request.data['password'],salt=None,hasher='default')
                    request.data['password']=Django_hashed_password
                    #user_obj.password = hashed_pwd
                    #user_obj.save()
                    serializer=UserSerializer(data=request.data)
                    if(serializer.is_valid()):
                        serializer.save()
                        return Response({'succefully registered'},status=status.HTTP_200_OK)
        except:
            return Response({'please provide correct details'},status=status.HTTP_400_BAD_REQUEST)     


class login(APIView):
     def post(self,request):        
        try:
            if(request.data['username'] and request.data['password']):
                check=authenticate(username=request.data['username'],password=request.data['password'])
                #check=User.objects.filter(username=request.data['username'],password=request.data['password'])         
                #print(check)
                if (check):              
                    token,created=Token.objects.get_or_create(user=check)
                    return Response({"token":token.key},status=status.HTTP_200_OK)               
                else:
                    return Response({"wrong credentials"},status=status.HTTP_400_BAD_REQUEST)  
        except:
            return Response({"please provide username and password"},status=status.HTTP_400_BAD_REQUEST)         
              


class logout(APIView):
     authentication_classes=(TokenAuthentication,)
     def post(self,request):   
         try:      
             request.user.auth_token.delete()
             return Response({"logged out succefully"},status=status.HTTP_200_OK)
         except:
             return Response({"provide token"},status=status.HTTP_400_BAD_REQUEST)
    
"""
class Comment(object):
    def __init__(self, num1, num2, op,ans):
        self.num1 = num1
        self.num2 = num2
        self.op = op
        self.ans=ans
"""

#@api_view(['GET','POST','PUT','DELETE'])
class calc(APIView):    
    authentication_classes=[TokenAuthentication]
    permission_classes=[IsAuthenticated]
    def get(self,request):  
            #calculator.objects.filter().delete()
            #print(request.GET)
        try:
            if(request.GET['num1'] and request.GET['num2']):  
                num1=request.GET['num1']
                num2=request.GET['num2'] 
                data=dict()
                data['num1']=num1
                data['num2']=num2
                if(calculator.objects.filter(num1=num1,num2=num2,op='+').count()==1):  
                        obj=calculator.objects.filter(num1=num1,num2=num2,op='+').values('ans')                
                        return Response({'Succesful from db  ans is ':obj[0]},status=status.HTTP_200_OK)                             
                else:
                        ans=int(num1)+int(num2)            
                        serializer=CalculatorSerializer(data=data)
                        data['op']='+'
                        data['ans']=int(ans)                     
                        if serializer.is_valid():                 
                              serializer.save() 
                              return Response({'Succesful ans is':ans},status=status.HTTP_200_OK)
        except:        
            return Response({'Unsuccesful please provide numbers'},status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request): 
        try:
            if(request.data['num1'] and request.data['num2']): 
                num1=request.data['num1']
                num2=request.data['num2']
                #print(num1)                
                if(calculator.objects.filter(num1=num1,num2=num2,op='/').count()==1):  
                        obj=calculator.objects.filter(num1=num1,num2=num2,op='/').values('ans')
                        return Response({'Succesful from db  ans is ':obj[0]},status=status.HTTP_200_OK)                             
                else:
                    ans=int(num1)/int(num2)
                    #print(type(ans))
                    #data=request.data
                    request.data['op']='/'
                    request.data['ans']=int(ans)
                    serializer=CalculatorSerializer(data=request.data)                               
                    if serializer.is_valid():                 
                        serializer.save()                 
                        return Response({'Succesful ans is':int(ans)},status=status.HTTP_200_OK)  
        except:
            return Response({'Unsuccesful please provide numbers'},status=status.HTTP_400_BAD_REQUEST)

    def post(self,request):        
        try:   
            if(request.data['num1'] and request.data['num2']): 
                num1=request.data['num1']
                num2=request.data['num2']
                #print(num1)                
                if(calculator.objects.filter(num1=num1,num2=num2,op='*').count()==1):  
                        obj=calculator.objects.filter(num1=num1,num2=num2,op='*').values('ans')              
                        return Response({'Succesful from db  ans is ':obj[0]},status=status.HTTP_200_OK)                             
                else:
                    ans=int(num1)*int(num2)
                    #print(type(ans))
                    #data=request.data
                    request.data['op']='*'
                    request.data['ans']=int(ans)
                    serializer=CalculatorSerializer(data=request.data)                               
                    if serializer.is_valid():                 
                        serializer.save()                 
                        return Response({'Succesful ans is':ans},status=status.HTTP_200_OK)  
        except:
            return Response({'Unsuccesful please provide numbers'},status=status.HTTP_400_BAD_REQUEST)

    def put(self,request):     
        try:      
            if(request.data['num1'] and request.data['num2']): 
                num1=request.data['num1']
                num2=request.data['num2']                
                if(calculator.objects.filter(num1=num1,num2=num2,op='-').count()==1):  
                        obj=calculator.objects.filter(num1=num1,num2=num2,op='-').values('ans')                
                        return Response({'Succesful from db  ans is ':obj[0]},status=status.HTTP_200_OK)                             
                else:
                    ans=int(num1)-int(num2)
                    #print(type(ans))
                    #data=request.data
                    request.data['op']='-'
                    request.data['ans']=int(ans)
                    serializer=CalculatorSerializer(data=request.data)                               
                    if serializer.is_valid():                 
                        serializer.save()                 
                        return Response({'Succesful ans is':int(ans)},status=status.HTTP_200_OK)  
        except:
            return Response({'Unsuccesful please provide numbers'},status=status.HTTP_400_BAD_REQUEST)
              
                


