from rest_framework import serializers
from .models import calculator
from django.contrib.auth.models import  User


class CalculatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = calculator
        fields = ['num1','num2','op','ans']




class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields=['username','password','email','first_name','last_name']


        